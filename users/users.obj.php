<?php

	class SimpleUsers
	{

		private $mysqli, $stmt;
		private $sessionName = "SimpleUsers";
		public $logged_in = false;
		public $userdata;
        public $email;

		public function __construct()
		{
			$sessionId = session_id();
			if( strlen($sessionId) == 0)
				throw new Exception("No session has been started.\n<br />Please add `session_start();` initially in your file before any output.");

			$this->mysqli = new mysqli($GLOBALS["mysql_hostname"], $GLOBALS["mysql_username"], $GLOBALS["mysql_password"], $GLOBALS["mysql_database"]);
			if( $this->mysqli->connect_error )
				throw new Exception("MySQL connection could not be established: ".$this->mysqli->connect_error);

			$this->_validateUser();
			$this->_populateUserdata();
			$this->_updateActivity();
		}

		public function createUser($email, $password)
		{
			$salt = $this->_generateSalt();
			$password = $salt.$password;

			$sql = "INSERT INTO users VALUES (NULL, ?, SHA1(?), ?, NOW(), NOW())";
			if( !$this->stmt = $this->mysqli->prepare($sql) )
				throw new Exception("MySQL Prepare statement failed: ".$this->mysqli->error);

			$this->stmt->bind_param("sss", $email, $password, $salt);
			if( $this->stmt->execute()) {
                $user_id = $this->stmt->insert_id;

                $sql = "INSERT INTO users_profile VALUES (?, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)";
                if( !$this->stmt = $this->mysqli->prepare($sql) )
                    throw new Exception("MySQL Prepare statement failed: ".$this->mysqli->error);

                $this->stmt->bind_param("i", $user_id);

                return $user_id;
            }

            return false;
		}

		public function loginUser( $email, $password )
		{
			$sql = "SELECT user_id FROM users WHERE email=? AND SHA1(CONCAT(uSalt, ?))=uPassword LIMIT 1";
			if( !$this->stmt = $this->mysqli->prepare($sql) )
				throw new Exception("MySQL Prepare statement failed: ".$this->mysqli->error);

			$this->stmt->bind_param("ss", $email, $password);
			$this->stmt->execute();
			$this->stmt->store_result();

			if( $this->stmt->num_rows == 0)
				return false;

			$this->stmt->bind_result($user_id);
			$this->stmt->fetch();

			$_SESSION[$this->sessionName]["user_id"] = $user_id;
            $_SESSION[$this->sessionName]["email"] = $email;

            $this->logged_in = true;

			return $user_id;
		}

        public function getEmail() {
            return $_SESSION[$this->sessionName]["email"];
        }


        public function getProfile() {
            $user_id = $_SESSION[$this->sessionName]["user_id"];

            $sql = "SELECT * FROM users_profile WHERE user_id=? LIMIT 1";

            if( !$this->stmt = $this->mysqli->prepare($sql) )
                throw new Exception("MySQL Prepare statement failed: ".$this->mysqli->error);

            $this->stmt->bind_param("i", $user_id);
            $this->stmt->execute();
            $this->stmt->store_result();

            if( $this->stmt->num_rows == 0) {
                return false;
            };

            $this->stmt->bind_result($user_id, $registration_id, $title, $first_name, $last_name, $role, $city,
                                     $country, $attendee_type, $department, $affiliation, $phone_number, $fax_number,
                                     $registration_date, $payment_status, $remarks);
            $this->stmt->fetch();

            $profile = array(
                "registration_id" => $registration_id,
                "title" => $title,
                "first_name" => $first_name,
                "last_name" => $last_name,
                "role" => $role,
                "city" => $city,
                "country" => $country,
                "attendee_type" => $attendee_type,
                "department" => $department,
                "affiliation" => $affiliation,
                "phone_number" => $phone_number,
                "fax_number" => $fax_number,
                "registration_date" => $registration_date,
                "payment_status" => $payment_status,
                "remarks" => $remarks,
            );

            return $profile;
        }

        public function updateProfile($profile) {
            if( !$this->logged_in )
                return;

            $user_id = $_SESSION[$this->sessionName]["user_id"];

            $sql = "UPDATE users_profile
                    SET first_name = ?,
                        last_name = ?,
                        country = ?,
                        city = ?,
                        phone_number = ?,
                        fax_number = ?,
                        department = ?,
                        affiliation = ?,
                        remarks = ?
                    WHERE user_id=?";

            if( !$this->stmt = $this->mysqli->prepare($sql) )
                throw new Exception("MySQL Prepare statement failed: ".$this->mysqli->error);

            $this->stmt->bind_param("sssssssssi",
                                    $profile["first_name"], $profile["last_name"], $profile["country"],
                                    $profile["city"],  $profile["phone_number"], $profile["fax_number"], $profile["department"],
                                    $profile["affiliation"], $profile["remarks"],
                                    $user_id);
            $this->stmt->execute();
            return;
        }


		/**
		* Logout the active user, unsetting the userId session.
		* This is a void function
		*/

		public function logoutUser()
		{
			if( isset($_SESSION[$this->sessionName]) )
				unset($_SESSION[$this->sessionName]);

			$this->logged_in = false;
		}

        public function setInfo( $key, $value, $user_id = null)
        {
            if($user_id == null)
            {
                if( !$this->logged_in )
                    return false;
            }
            $reservedKeys = array("user_id", "email", "uActivity", "uCreated", "uLevel");
            if( in_array($key, $reservedKeys) )
                throw new Exception("User information key \"".$key."\" is reserved for internal use!");
            if( $user_id == null )
                $user_id = $_SESSION[$this->sessionName]["user_id"];
            if( $this->getInfo($key, $user_id) )
            {
                $sql = "UPDATE users_information SET infoValue=? WHERE infoKey=? AND user_id=? LIMIT 1";
                if( !$this->stmt = $this->mysqli->prepare($sql) )
                    throw new Exception("MySQL Prepare statement failed: ".$this->mysqli->error);
                $this->stmt->bind_param("ssi", $value, $key, $user_id);
                $this->stmt->execute();
            }
            else
            {
                $sql = "INSERT INTO users_information VALUES (?, ?, ?)";
                if( !$this->stmt = $this->mysqli->prepare($sql) )
                    throw new Exception("MySQL Prepare statement failed: ".$this->mysqli->error);
                $this->stmt->bind_param("iss", $user_id, $key, $value);
                $this->stmt->execute();
            }
            return true;
        }
        public function getInfo( $key, $user_id = null )
        {
            if( $user_id == null )
            {
                if( !$this->logged_in )
                    return false;
                $user_id = $_SESSION[$this->sessionName]["user_id"];
            }
            $sql = "SELECT infoValue FROM users_information WHERE user_id=? AND infoKey=? LIMIT 1";
            if( !$this->stmt = $this->mysqli->prepare($sql) )
                throw new Exception("MySQL Prepare statement failed: ".$this->mysqli->error);
            $this->stmt->bind_param("is", $user_id, $key);
            $this->stmt->execute();
            $this->stmt->store_result();
            if( $this->stmt->num_rows == 0)
                return "";
            $this->stmt->bind_result($value);
            $this->stmt->fetch();
            return $value;
        }
        public function removeInfo( $key, $user_id = null )
        {
            if( $user_id == null )
            {
                if( !$this->logged_in )
                    return false;
                $user_id = $_SESSION[$this->sessionName]["user_id"];
            }
            $sql = "DELETE FROM users_information WHERE user_id=? AND infoKey=? LIMIT 1";
            if( !$this->stmt = $this->mysqli->prepare($sql) )
                throw new Exception("MySQL Prepare statement failed: ".$this->mysqli->error);
            $this->stmt->bind_param("is", $user_id, $key);
            $this->stmt->execute();
            if( $this->stmt->affected_rows > 0)
                return true;
            return false;
        }

        public function getInfoArray( $user_id = null )
        {
            if( $user_id == null )
                $user_id = $_SESSION[$this->sessionName]["user_id"];
            $sql = "SELECT infoKey, infoValue FROM users_information WHERE user_id=? ORDER BY infoKey ASC";
            if( !$this->stmt = $this->mysqli->prepare($sql) )
                throw new Exception("MySQL Prepare statement failed: ".$this->mysqli->error);
            $this->stmt->bind_param("i", $user_id);
            $this->stmt->execute();
            $this->stmt->store_result();
            $userInfo = array();
            if( $this->stmt->num_rows > 0)
            {
                $this->stmt->bind_result($key, $value);
                while( $this->stmt->fetch() )
                    $userInfo[$key] = $value;
            }

            $user = $this->getSingleUser($user_id);
            $userInfo = array_merge($userInfo, $user);
            asort($userInfo);
            return $userInfo;
        }

		public function setPassword( $password, $user_id = null )
		{

			if( $user_id == null )
				$user_id = $_SESSION[$this->sessionName]["user_id"];

			$salt = $this->_generateSalt();
			$password = $salt.$password;

			$sql = "UPDATE users SET uPassword=SHA1(?), uSalt=? WHERE user_id=? LIMIT 1";
			if( !$this->stmt = $this->mysqli->prepare($sql) )
				throw new Exception("MySQL Prepare statement failed: ".$this->mysqli->error);

			$this->stmt->bind_param("ssi", $password, $salt, $user_id);
			return $this->stmt->execute();
		}


		public function getUsers()
		{
			
			$sql = "SELECT DISTINCT user_id, email, uActivity, uCreated FROM users ORDER BY email ASC";

			if( !$this->stmt = $this->mysqli->prepare($sql) )
				throw new Exception("MySQL Prepare statement failed: ".$this->mysqli->error);

			$this->stmt->execute();
			$this->stmt->store_result();

			if( $this->stmt->num_rows == 0)
				return array();

			$this->stmt->bind_result($user_id, $email, $activity, $created);

			$users = array();

			$i = 0;
			while( $this->stmt->fetch() )
			{		
				$users[$i]["user_id"] = $user_id;
				$users[$i]["email"] = $email;
				$users[$i]["uActivity"] = $activity;
				$users[$i]["uCreated"] = $created;

				$i++;
			}

			return $users;

		}

		public function getSingleUser( $user_id = null )
		{

			if( $user_id == null )
				$user_id = $_SESSION[$this->sessionName]["user_id"];

			$sql = "SELECT user_id, email, uActivity, uCreated FROM users WHERE user_id=? LIMIT 1";
			if( !$this->stmt = $this->mysqli->prepare($sql) )
				throw new Exception("MySQL Prepare statement failed: ".$this->mysqli->error);

			$this->stmt->bind_param("i", $user_id);
			$this->stmt->execute();
			$this->stmt->store_result();

			if( $this->stmt->num_rows == 0)
				return false;

			$this->stmt->bind_result($user_id, $email, $activity, $created);
			$this->stmt->fetch();

			$user["user_id"] = $user_id;
			$user["email"] = $email;
			$user["uActivity"] = $activity;
			$user["uCreated"] = $created;

			return $user;

		}


		public function deleteUser( $user_id )
		{
			$sql = "DELETE FROM users WHERE user_id=?";
			if( !$this->stmt = $this->mysqli->prepare($sql) )
				throw new Exception("MySQL Prepare statement failed: ".$this->mysqli->error);

			$this->stmt->bind_param("i", $user_id);
			$this->stmt->execute();

			$sql = "DELETE FROM users_information WHERE user_id=?";
			if( !$this->stmt = $this->mysqli->prepare($sql) )
				throw new Exception("MySQL Prepare statement failed: ".$this->mysqli->error);

			$this->stmt->bind_param("i", $user_id);
			$this->stmt->execute();

			return;
		}

		

		/**
		* This function updates the users last activity time
		* This is a void function.
		*/

		private function _updateActivity()
		{
			if( !$this->logged_in )
				return;

			$userId = $_SESSION[$this->sessionName]["user_id"];

			$sql = "UPDATE users SET uActivity=NOW() WHERE user_id=? LIMIT 1";
			if( !$this->stmt = $this->mysqli->prepare($sql) )
				throw new Exception("MySQL Prepare statement failed: ".$this->mysqli->error);

			$this->stmt->bind_param("i", $user_id);
			$this->stmt->execute();
			return;
		}

		/**
		* Validates if the user is logged in or not.
		* This is a void function.
		*/

		private function _validateUser()
		{
			if( !isset($_SESSION[$this->sessionName]["user_id"]) )
				return;

			if( !$this->_validateUserId() )
				return;

			$this->logged_in = true;
		}

		/**
		* Validates if the user id, in the session is still valid.
		*
		* @return	Returns (bool)true or false
		*/

		private function _validateUserId()
		{
			$user_id = $_SESSION[$this->sessionName]["user_id"];

			$sql = "SELECT user_id FROM users WHERE user_id=? LIMIT 1";
			if( !$this->stmt = $this->mysqli->prepare($sql) )
				throw new Exception("MySQL Prepare statement failed: ".$this->mysqli->error);

			$this->stmt->bind_param("i", $user_id);
			$this->stmt->execute();
			$this->stmt->store_result();

			if( $this->stmt->num_rows == 1)
				return true;

			$this->logoutUser();

			return false;
		}
		
		/**
		* Populates the current users data information for 
		* quick access as an object.
		*
		* @return void
		*/	
		
		private function _populateUserdata()
		{
			$this->userdata = array();
			
			if( $this->logged_in )
			{
				$user_id = $_SESSION[$this->sessionName]["user_id"];
				$data = $this->getInfoArray();
				foreach($data as $key => $value)
					$this->userdata[$key] = $value;
			}
		}

		/**
		* Generates a 128 len string used as a random salt for
		* securing you oneway encrypted password
		*
		* @return String with 128 characters
		*/

		private function _generateSalt()
		{
			$salt = null;

			while( strlen($salt) < 128 )
				$salt = $salt.uniqid(null, true);

			return substr($salt, 0, 128);
		}

        public function load_assigments() {
            $user_id = $_SESSION[$this->sessionName]["user_id"];

            $result = $this->mysqli->query(
                "SELECT paper_id FROM assignment
              WHERE  reviewer_id = $user_id"
            );

            if($result) {
                return $result->fetch_all();
            } else {
                return [];
            }
        }

        public function get_paper($paper_id) {
            $result = $this->mysqli->query(
                "SELECT * FROM paper
                 WHERE  paper_id = $paper_id"
            );

            if($result) {
                return $result->fetch_assoc();
            } else {
                return array();
            }
        }

        public function get_paper_myself() {
            $user_id = $_SESSION[$this->sessionName]["user_id"];

            $result = $this->mysqli->query(
                "SELECT * FROM paper
                 WHERE  author_id = $user_id"
            );

            if($result) {
                return $result->fetch_assoc();
            } else {
                return array();
            }
        }

        public function get_my_id() {
            return $_SESSION[$this->sessionName]["user_id"];
        }


        public function get_all_papers() {
            $result = $this->mysqli->query(
                "SELECT * FROM paper"
            );

            if($result) {
                $all = $result->fetch_all();
                foreach($all as $key => $value) {
                    $all[$key] = array (
                        "paper_id" => $value[0],
                        "paper_filename" => $value[1],
                        "key_words" => $value[2],
                        "title" => $value[3],
                        "accepted" => $value[4],
                        "author_id" => $value[5],
                    );
                }
                return $all;
            } else {
                return array();
            }
        }

        public function get_all_reviews($paper_id) {
            $result = $this->mysqli->query(
                "SELECT * FROM review
                 WHERE  paper_id = $paper_id"
            );

            if($result) {
                $all = $result->fetch_all();
                foreach($all as $key => $value) {
                    $all[$key] = array (
                        "reviewer_id" => $value[1],
                        "score" => $value[2],
                        "comment" => $value[3],
                        "date" => $value[4],
                    );
                }
                return $all;
            } else {
                return array();
            }
        }

        public function insert_review($review) {
            $user_id = $_SESSION[$this->sessionName]["user_id"];
            $paper_id = $review["paper_id"];
            $score = $review["score"];
            $comment = $review["comment"];

            $sql =
                "INSERT INTO review
                 VALUES (?, ?, ?, ?, NOW())";

            if( !$this->stmt = $this->mysqli->prepare($sql) )
                throw new Exception("MySQL Prepare statement failed: ".$this->mysqli->error);

            $this->stmt->bind_param("iiss", $paper_id, $user_id, $score, $comment);
            return $this->stmt->execute();
        }

        public function update_review($review) {
            $user_id = $_SESSION[$this->sessionName]["user_id"];
            $paper_id = $review["paper_id"];
            $score = $review["score"];
            $comment = $review["comment"];

            $sql =
                "UPDATE review
                 SET    score = ?,
                        comment = ?
                 WHERE (paper_id = ?
                        AND reviewer_id = ?)";

            if( !$this->logged_in )
                return;

            if( !$this->stmt = $this->mysqli->prepare($sql) )
                throw new Exception("MySQL Prepare statement failed: ".$this->mysqli->error);

            $this->stmt->bind_param("ssii",
                $score,
                $comment,
                $paper_id,
                $user_id);

            $this->stmt->execute();
            return;
        }

        public function revoke($paper_id) {
            $this->mysqli->query(
                "UPDATE paper
                 SET    accepted = FALSE
                 WHERE  paper_id = $paper_id"
            );
        }

        public function accept($paper_id) {
            $this->mysqli->query(
                "UPDATE paper
                 SET    accepted = TRUE
                 WHERE  paper_id = $paper_id"
            );
        }

        public function get_total_users() {
            $result = $this->mysqli->query("SELECT COUNT(*) FROM users");
            return $result->fetch_row()[0];
        }

        public function get_total_submissions() {
            $result = $this->mysqli->query("SELECT COUNT(*) FROM paper");
            return $result->fetch_row()[0];
        }

        public function get_total_reviews() {
            $result = $this->mysqli->query("SELECT COUNT(*) FROM review");
            return $result->fetch_row()[0];
        }

        public function get_messages() {
            $user_id = $_SESSION[$this->sessionName]["user_id"];
            $result = $this->mysqli->query(
                "SELECT * FROM messages
                 WHERE  to_id = $user_id"
            );


            if($result) {
                $messages = $result->fetch_all();
                $ret = [];

                foreach($messages as $msg) {
                    $from_id = $msg[1];
                    $result = $this->mysqli->query(
                        "SELECT title, first_name, last_name FROM users_profile
                         WHERE  user_id = $from_id"
                    );
                    $from_user = $result->fetch_assoc();
                    $from_name= $from_user["title"]." ".$from_user["first_name"]." ".$from_user["last_name"];

                    array_push($ret, array (
                        "from_name" => $from_name,
                        "send_date" => $msg[3],
                        "content" => $msg[4],
                        "is_read" => $msg[5],
                    ));

                    $this->mysqli->query(
                        "UPDATE messages
                         SET    is_read = TRUE
                         WHERE  message_id= $msg[0]"
                    );
                }

                return $ret;
            } else {
                return array();
            }
        }

        public function send_message($message) {

        }

	}
	
?>