<?php
// Base User

$tables["users"] = "CREATE TABLE IF NOT EXISTS `users` (
`user_id` INT NOT NULL auto_increment,
`email` VARCHAR(128),
`uPassword` VARCHAR(40) NOT NULL,
`uSalt` VARCHAR(128) NOT NULL,
`uActivity` VARCHAR(128) NOT NULL,
`uCreated` DATETIME NOT NULL,
PRIMARY KEY (`user_id`),
UNIQUE KEY `email_UNIQUE` (`email`),
UNIQUE KEY `user_id_UNIQUE` (`user_id`))
ENGINE = MyISAM AUTO_INCREMENT = 1";

$tables["users_information"] = "CREATE TABLE IF NOT EXISTS `users_information` (
  `user_id` int(11) NOT NULL,
  `infoKey` varchar(128) NOT NULL,
  `InfoValue` text NOT NULL,
  KEY `user_id` (`user_id`)
  ) ENGINE=MyISAM;";


$tables["users_profile"] = "CREATE TABLE IF NOT EXISTS `users_profile` (
`user_id` INT NOT NULL,
`registration_id` VARCHAR(30),
`title` VARCHAR(10),
`first_name` VARCHAR(30),
`last_name` VARCHAR(30),
`role` VARCHAR(15),
`city` VARCHAR(45),
`country` VARCHAR(45),
`attendee_type` VARCHAR(10),
`department` VARCHAR(25),
`affiliation` VARCHAR(45),
`phone_number` VARCHAR(45),
`fax_number` VARCHAR(45),
`registration_date` DATETIME,
`payment_status` VARCHAR(10),
`remarks` VARCHAR(225),
PRIMARY KEY (`user_id`),
FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`))
ENGINE = MyISAM";
// Four Roles

$tables["author"] = "CREATE TABLE IF NOT EXISTS `author` (
`user_id` INT NOT NULL,
`paper_id` INT NOT NULL,
PRIMARY KEY (`user_id`),
FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`),
FOREIGN KEY (`paper_id`) REFERENCES `paper` (`paper_id`))
ENGINE = MyISAM";

$tables["reviewer"] = "CREATE TABLE IF NOT EXISTS `reviewer` (
`user_id` INT NOT NULL,
PRIMARY KEY (`user_id`),
FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`))
ENGINE = MyISAM AUTO_INCREMENT = 1";

$tables["administrator"] = "CREATE TABLE IF NOT EXISTS `administrator` (
`user_id` INT NOT NULL,
PRIMARY KEY (`user_id`),
FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`))
ENGINE = MyISAM";

$tables["manager"] = "CREATE TABLE IF NOT EXISTS `manager` (
`user_id` INT NOT NULL,
PRIMARY KEY (`user_id`),
FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`))
ENGINE = MyISAM";

// Paper

$tables["paper"] = "CREATE TABLE IF NOT EXISTS `paper` (
`paper_id`  INT NOT NULL auto_increment,
`paper_filename` VARCHAR(25),
`key_words` VARCHAR(225),
`title`     VARCHAR(25) NULL,
`accepted`  TINYINT(1)  NULL,
`author_id` INT         NOT NULL,
`pdf`       BLOB        NULL,
PRIMARY KEY (`paper_id`),
FOREIGN KEY (`author_id`) REFERENCES `author` (`user_id`))
ENGINE = MyISAM AUTO_INCREMENT = 1";

// Review

$tables["assignment"] = "CREATE TABLE IF NOT EXISTS `assignment` (
`paper_id`    INT NOT NULL,
`reviewer_id` INT NOT NULL,
PRIMARY KEY (`paper_id`, `reviewer_id`),
FOREIGN KEY (`paper_id`)    REFERENCES `paper`    (`paper_id`),
FOREIGN KEY (`reviewer_id`) REFERENCES `reviewer` (`user_id`))
ENGINE = MyISAM";

$tables["review"] = "CREATE TABLE IF NOT EXISTS `review` (
`paper_id`    INT NOT NULL,
`reviewer_id` INT NOT NULL,
`score`       VARCHAR(45) NULL,
`comment`     VARCHAR(225) NULL,
`date`        DATETIME NULL,
PRIMARY KEY (`paper_id`, `reviewer_id`),
FOREIGN KEY (`paper_id`)    REFERENCES `paper`    (`paper_id`),
FOREIGN KEY (`reviewer_id`) REFERENCES `reviewer` (`user_id`))
ENGINE = MyISAM";

// Messages

$tables["messages"] = "CREATE TABLE IF NOT EXISTS `messages` (
`message_id`  INT NOT NULL auto_increment,
`from_id`     INT NOT NULL,
`to_id`       VARCHAR(45) NULL,
`sent_date`   DATETIME NULL,
`content`     VARCHAR(255) NULL,
`is_read`     TINYINT(1)   NULL,
PRIMARY KEY (`message_id`),
FOREIGN KEY (`from_id`) REFERENCES `users` (`user_id`),
FOREIGN KEY (`to_id`)   REFERENCES `users` (`user_id`))
ENGINE = MyISAM";

?>