<?php

/**
 * Make sure you started your'e sessions!
 * You need to include su.inc.php to make SimpleUsers Work
 * After that, create an instance of SimpleUsers and your'e all set!
 */

session_start();
require_once(dirname(__FILE__) . "/users/su.inc.php");

$SimpleUsers = new SimpleUsers();

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>Conference System</title>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8"/>
    <?php require_once("header_inc.php"); ?>
    <link href="assets/css/index.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="assets/js/index.js"></script>
</head>
<body>
<div class="container">
    <nav class="navbar navbar-default">
        <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <a class="navbar-brand" href="index.php">HomePage</a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li><a href="#">Program</a></li>
                    <li><a href="#">Committee</a></li>
                </ul>

                <ul class="nav navbar-nav navbar-right">
                    <?php
                        if($SimpleUsers->logged_in) {
                            echo "<li><a href=\"logout.php\">Logout</a></li>
                                  <li><a href=\"userinfo.php\">UserInfo</a></li>";
                        } else {
                            echo "<li><a href=\"login.php\">Login</a></li>
                                  <li><a href=\"newuser.php\">Register</a></li>";
                        }
                    ?>
                </ul>
            </div><!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->
    </nav>
    <div class="jumbotron" style="margin-top: 30px">
        <h1>2nd Global Tourism & Hospitality Conference</h1>
        <p>16-18 May 2016 Hotel ICON, Hong Kong</p><p>Held by School of Hotel and Tourism Management of The Hong Kong Polytechnic University</p>
        <p>
            <a class="btn btn-lg btn-primary" href="newuser.php" role="button">Register Now</a>
        </p>
    </div>
    <div class="row">
        <div class="col-sm-4">
            <img src="assets/img/01.jpeg" alt="" width="100%"/>
        </div>
        <div class="col-sm-4">
            <img src="assets/img/02.jpeg" alt="" width="100%"/>
        </div>
        <div class="col-sm-4">
            <img src="assets/img/03.jpg" alt="" width="100%"/>
        </div>
    </div>
</div>
</body>
</html>