<?php

if(isset($_GET['paper_id'])) {
    session_start();
    require_once(dirname(__FILE__) . "/../users/su.inc.php");

    $SimpleUsers = new SimpleUsers();

    if (!$SimpleUsers->logged_in) {
        header("Location: login.php");
        exit;
    }

    if($SimpleUsers->getProfile()["role"] == "manager") {
        $SimpleUsers->revoke($_GET['paper_id']);
        header("Location: ../userinfo.php?page=special");
        exit;
    } else {
        header("Location: ../userinfo.php"); // XXX: Should display error ...
        exit;
    }
}

?>