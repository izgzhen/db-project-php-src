<?php

	session_start();
	require_once(dirname(__FILE__) . "/users/su.inc.php");

	$SimpleUsers = new SimpleUsers();

	// This is a simple way of validating if a user is logged in or not.
	// If the user is logged in, the value is (bool)true - otherwise (bool)false.
	if( !$SimpleUsers->logged_in )
	{
		header("Location: login.php");
		exit;
	}

	// If the user is logged in, we can safely proceed.
	$user_id = $_GET["user_id"];

	//Delete the user (plain and simple)
	$SimpleUsers->deleteUser($user_id);
	header("Location: users.php");

?>