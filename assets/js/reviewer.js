var selectedPaper = -1;
var selectedPaperId = -1;

function toggle(x, all, id) {
    for(var i = 0; i < all; i++) {
        $("#btn-"+i).toggleClass("active", false);
        $("#drawer-"+i).toggleClass("hidden", true);
    }

    $("#btn-"+x).toggleClass("active");
    $("#drawer-"+x).toggleClass("hidden");

    selectedPaper = x;
    selectedPaperId = id;

    $("#submit-btn").toggleClass("hidden", false);

    $("#paper-id-input").val(selectedPaperId);

    $("#score-input").val($("#score-drawer-"+x).text().trim());
    $("#comment-input").val($("#comment-drawer-"+x).text().trim());
}


