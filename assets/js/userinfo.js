function init() {
    $("#profile-info-form").hide();


}

function changeEditable() {
    $("#profile-info-static").hide();
    $("#profile-info-form").show();
}

function showMsg(i) {
    $("#msg-content-"+i).toggleClass("hidden");
    $("#unread-"+i).hide();
}