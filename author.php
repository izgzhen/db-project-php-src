<?php
// Example: Author View

$paper = $SimpleUsers->get_paper_myself();

$reviews = $SimpleUsers->get_all_reviews($paper["paper_id"]);

?>

<ul>
    <h3>Received Reviews of "<?php echo $paper["title"]; ?>"</h3>
    <hr>
    <div class="list-group">
        <li class="list-group-item">
        <?php foreach($reviews as $key=>$value): ?>
            <h4> Review #<?php echo $key; ?></h4>
            <p>
                Score: <b><?php echo $value["score"]; ?>/5</b>
                <br>
                Comment: <br>
                <div class="well"><?php echo $value["comment"]; ?></div>
            </p>
        <?php endforeach; ?>
        </li>

    </div>
</ul>