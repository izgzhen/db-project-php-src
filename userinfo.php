<?php

session_start();
require_once(dirname(__FILE__) . "/users/su.inc.php");

$SimpleUsers = new SimpleUsers();

// This is a simple way of validating if a user is logged in or not.
// If the user is logged in, the value is (bool)true - otherwise (bool)false.
if (!$SimpleUsers->logged_in) {
    header("Location: login.php");
    exit;
}

if (isset($_GET['page'])) {
    $page = $_GET['page'];
} else {
    $page = "profile";
}

$messages = $SimpleUsers->get_messages();


$profile = $SimpleUsers->getProfile();

$email = $SimpleUsers->getEmail();

$role = $profile["role"];


$address = $profile["city"].", ".$profile["country"];

if (isset($_POST['first-name'])) {
    $profile["first_name"] = $_POST["first-name"];
    $profile["last_name"] = $_POST["last-name"];
    $profile["country"] = $_POST["country"];
    $profile["city"] = $_POST["city"];
    $profile["phone_number"] = $_POST["phone-number"];
    $profile["fax_number"] = $_POST["fax-number"];
    $profile["department"] = $_POST["department"];
    $profile["affiliation"] = $_POST["affiliation"];
    $profile["remarks"] = $_POST["remarks"];
    $SimpleUsers->updateProfile($profile);

    header("Location: userinfo.php");
    exit;
}


?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title><?php echo $page ?></title>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8"/>
    <?php require_once("header_inc.php"); ?>
    <script type="application/javascript" src="assets/js/userinfo.js"></script>
    <script type="application/javascript" src="assets/js/<?php echo $role; ?>.js"></script>
</head>
<body onload="init();">

<?php if (isset($error)): ?>
    <p>
        <?php echo $error; ?>
    </p>
<?php endif; ?>

<div class="container">
    <nav class="navbar navbar-default">
        <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <a class="navbar-brand" href="index.php">Homepage</a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li><a href="#">Program</a></li>
                    <li><a href="#">Committee</a></li>
                </ul>

                <ul class="nav navbar-nav navbar-right">
                    <li><a href="logout.php">Logout</a></li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container-fluid -->
    </nav>
    <div class="row">
        <div class="col-sm-3">
            <ul class="nav nav-stacked">
                <li role="presentation"
                    <?php if ($page == "profile") echo "class=\"active\""; ?>
                    ><a href="userinfo.php?page=profile">Profile</a></li>
                <li role="presentation"
                    <?php if ($page == "security") echo "class=\"active\""; ?>
                    ><a href="userinfo.php?page=security">Security</a></li>
                <li role="presentation"
                    <?php if ($page == "special") echo "class=\"active\""; ?>
                    ><a href="userinfo.php?page=special">Management Panel</a></li>
                <li role="presentation"
                    <?php if ($page == "messages") echo "class=\"active\""; ?>
                    ><a href="userinfo.php?page=messages">Messages</a></li>
            </ul>
        </div>
        <div class="col-sm-8">
            <?php if ($page == "profile"): ?>
                <!-- Profile -->
                <h3>Profile</h3>
                <hr>
                <div class="well well-sm">
                    <div class="row">
                        <div class="col-sm-6 col-md-4">
                            <img src="assets/img/doge.jpg" alt="" class="img-rounded img-responsive"/>
                        </div>
                        <div class="col-sm-6 col-md-8">

                            <div id="profile-info-static">
                                <h3><?php echo $profile["title"]." ".$profile["first_name"]." ".$profile["last_name"]; ?></h3>
                                Email: <?php echo $email; ?>
                                <br>
                                Address:
                                <cite title="<?php echo $address; ?>">
                                     <?php echo $address; ?>
                                </cite>
                                <br>
                                Registration ID: <?php echo $profile["registration_id"]; ?>
                                <br>
                                Role: <?php echo $profile["role"]; ?>
                                <br>
                                Attendee Type: <?php echo $profile["attendee_type"]; ?>
                                <br>
                                Department: <?php echo $profile["department"]; ?>
                                <br>
                                Affiliation: <?php echo $profile["affiliation"]; ?>
                                <br>
                                Phone Number: <?php echo $profile["phone_number"]; ?>
                                <br>
                                Fax Number: <?php echo $profile["fax_number"]; ?>
                                <br>
                                Registration Date: <?php echo $profile["registration_date"]; ?>
                                <br>
                                Payment Status: <?php echo $profile["payment_status"]; ?>
                                <br>
                                Remarks: <?php echo $profile["remarks"]; ?>
                                <br>
                                <button class="btn btn-primary" onclick="changeEditable();">Click to edit</button>
                            </div>
                            <div id="profile-info-form">
                                <form action="" method="post" id="profile-info-form">
                                    First Name: <input type="text" name="first-name" value="<?php echo $profile["first_name"]; ?>">
                                    <br>
                                    Last Name: <input type="text" name="last-name" value="<?php echo $profile["last_name"]; ?>">
                                    <br>
                                    Country: <input type="text" name="country" value="<?php echo $profile["country"]; ?>">
                                    <br>
                                    City: <input type="text" name="city" value="<?php echo $profile["city"]; ?>">
                                    <br>
                                    Phone Number: <input type="text" name="phone-number" value="<?php echo $profile["phone_number"]; ?>">
                                    <br>
                                    Fax Number: <input type="text" name="fax-number" value="<?php echo $profile["fax_number"]; ?>">
                                    <br>
                                    Department: <input type="text" name="department" value="<?php echo $profile["department"]; ?>">
                                    <br>
                                    Affiliation: <input type="text" name="affiliation" value="<?php echo $profile["affiliation"]; ?>">
                                    <br>
                                    Remarks: <input type="text" name="remarks" value="<?php echo $profile["remarks"]; ?>">
                                    <br>
                                    <input class="btn btn-primary" type="submit" value="submit">
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endif ?>

            <?php if ($page == "security"): ?>
                <!-- Security -->
                <h3>Account Security</h3>
                <hr>
                <a href="management/reset-pwd.php" type="button" class="btn btn-primary" name="reset-pwd">Reset
                    Password</a>
                <br>
            <?php endif ?>

            <?php if ($page == "special") {
                include ($role.".php");
            }
            ?>

            <?php if ($page == "messages"): ?>
                <!-- Messages -->
                <h3>Message Center</h3>
                <ul class="list-group">
                    <?php foreach($messages as $key => $value): ?>
                        <button type="button" class="list-group-item"
                                onclick="showMsg(<?php echo $key; ?>)"
                                style="margin-top: 20px;">
                            From: <?php echo $value["from_name"]; ?>
                            <?php if(!$value["is_read"]): ?>
                                <span class="badge" id="unread-<?php echo $key; ?>">unread</span>
                            <?php endif ?>
                        </button>
                        <div id="msg-content-<?php echo $key; ?>"
                             class="well hidden">
                            <?php echo $value["content"]; ?>
                        </div>
                    <?php endforeach ?>
                </ul>
            <?php endif ?>
        </div>
    </div>
</div>
</div>


</body>
</html>