<?php
// Example: Manager View

$review_paper_pairs = [];

$papers = $SimpleUsers->get_all_papers();


foreach($papers as $value) {
    $reviews = $SimpleUsers->get_all_reviews($value["paper_id"]);
    array_push($review_paper_pairs, array(
        "paper" => $value,
        "reviews" => $reviews,
        "avg" => null,
        "progress" => null,
    ));
}

// Calculation of avg score
$total_count = 0;
$at_least_reviews_per_paper = 4;

foreach ($review_paper_pairs as $key => $value) {
    $sum = 0;
    foreach($value["reviews"] as $i => $r) {
        $sum = $sum + $r["score"];
    }
    if(count($value["reviews"]))
        $review_paper_pairs[$key]["avg"] = $sum / count($value["reviews"]);
    else
        $review_paper_pairs[$key]["avg"] = "not rated";
    $review_paper_pairs[$key]["progress"] = count($value["reviews"]);
    $total_count = $total_count + count($value["reviews"]);
}

?>

<ul>
    <h3>All papers</h3>
    <hr>
    <div class="list-group">
        <class="list-group-item">
        <?php foreach($review_paper_pairs as $key=>$value): ?>
            <h4> Paper #<?php echo $value["paper"]["paper_id"]; ?></h4>

            <p>
                Avg. Score: <b><?php echo $value["avg"]; ?>/5</b>
                <br>
                Progress: <b><?php echo $value["progress"]."/".$at_least_reviews_per_paper; ?></b>
                <br><br>
                <?php if ($value["paper"]["accepted"]): ?>
                    Already accepted
                    <br>
                    <a href="api/revoke.php?paper_id=<?php echo $value["paper"]["paper_id"]; ?>"
                       class="btn btn-danger">Revoke acceptance</a>
                <?php else: ?>
                    <a href="api/accept.php?paper_id=<?php echo $value["paper"]["paper_id"]; ?>"
                       class="btn btn-success">Accept this paper</a>
                <?php endif ?>
            </p>
        <?php endforeach; ?>
        </li>
    </div>
    <hr>
    <p>
        Total Progress: <?php echo $total_count."/".(count($review_paper_pairs) * 4); ?>
    </p>
</ul>