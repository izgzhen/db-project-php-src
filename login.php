<?php

/**
 * Make sure you started your'e sessions!
 * You need to include su.inc.php to make SimpleUsers Work
 * After that, create an instance of SimpleUsers and your'e all set!
 */

session_start();
require_once(dirname(__FILE__) . "/users/su.inc.php");

$SimpleUsers = new SimpleUsers();

// Login from post data
if (isset($_POST["email"])) {

    // Attempt to login the user - if credentials are valid, it returns the users id, otherwise (bool)false.
    $res = $SimpleUsers->loginUser($_POST["email"], $_POST["password"]);
    if (!$res)
        $error = "You supplied the wrong credentials.";
    else {
        header("Location: userinfo.php");
        exit;
    }

} // Validation end

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title></title>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8"/>
    <?php require_once("header_inc.php"); ?>
</head>
<body>
<?php if (isset($error)): ?>
    <p>
        <?php echo $error; ?>
    </p>
<?php endif; ?>
<div class="container">
    <nav class="navbar navbar-default">
        <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <a class="navbar-brand" href="index.php">Homepage</a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li><a href="#">Program</a></li>
                    <li><a href="#">Committee</a></li>
                </ul>

                <ul class="nav navbar-nav navbar-right">
                    <li><a href="newuser.php">Register</a></li>
                </ul>
            </div><!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->
    </nav>
    <div class="modal-ish">
        <form action="" method="post">
            <div class="modal-header">
                <h2>Sign In</h2>
            </div>
            <div class="modal-body">
                <p>
                    <label>Email:</label>
                    <input type="text" name="email"/>
                </p>

                <p>
                    <label>Password:</label>
                    <input type="password" name="password"/>
                </p>
                <input type="submit" class="btn btn-primary" style="margin: 0 auto;" name="submit" value="Login"/>
            </div>
        </form>
    </div>
</div>

</body>
</html>