<?php
// Example: Admin View

$sys_report = array(
    "total_users" => $SimpleUsers->get_total_users(),
    "total_submissions" => $SimpleUsers->get_total_submissions(),
);

?>

<ul>
    <h3>System Report</h3>
    <hr>
    <div class="list-group">
        <li class="list-group-item">Total Users: <?php echo $sys_report["total_users"]; ?></li>
        <li class="list-group-item">Total Submissions: <?php echo $sys_report["total_submissions"]; ?></li>
    </div>
</ul>