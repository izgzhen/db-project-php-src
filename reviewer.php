<?php
// Example: Reviewer View


    $assignments = $SimpleUsers->load_assigments();
    $papers = [];
    $my_id = $SimpleUsers->get_my_id();

    foreach($assignments as $assignment) {
        foreach($assignment as $key => $value) {
            $pid = $value;
            $reviews = $SimpleUsers->get_all_reviews($pid);
            $review = null;

            foreach($reviews as $r) {
                if($r["reviewer_id"] == $my_id) {
                    $review = $r;
                }
            }

            if($review == null) {
                $review = array(
                    "score" => "N/A",
                    "comment" => "N/A",
                );
            }

            $paper = $SimpleUsers->get_paper($pid);

            array_push($papers, array(
                "paper" => $paper,
                "review" => $review,
            ));
        }
    }


    if(isset($_POST['score'])) {
        $review_post = array(
            "score"    => $_POST['score'],
            "paper_id" => $_POST['paper-id'],
            "comment"  => $_POST['comment'],
        );

//        $SimpleUsers->insert_review($review);
        $SimpleUsers->update_review($review_post);

        header("Location: userinfo.php?page=special");
        exit;
    }
?>


    <h4>Papers to review</h4>
        <?php foreach($papers as $key=>$value): ?>
            <button id="btn-<?php echo $key; ?>"
                    class="list-group-item"
                    onclick="toggle(<?php echo $key.", ".count($papers).", ".$value["paper"]['paper_id']; ?>)">
                Title: <?php echo $value["paper"]['title']; ?>
            </button>
            <div id="drawer-<?php echo $key; ?>" class="well hidden">
                Link: <a href="#">link</a>
                <br>
                Author: <?php echo $value["paper"]['author_id']; ?>
                <br>

                Previous Score:
                <div id="score-drawer-<?php echo $key; ?>">
                    <?php echo $value["review"]['score']; ?>
                </div>

                Previous Comment:
                <div id="comment-drawer-<?php echo $key; ?>">
                    <?php echo $value["review"]['comment']; ?>
                </div>
            </div>
        <?php endforeach; ?>
    <hr>
    <h4>Editor</h4>
    <form method="post" action="">
        Score: <input type="text" name="score" id="score-input">
        <br>
        Comment: <input type="text" name="comment" id="comment-input">
        <br>
        <input type="text" name="paper-id" id="paper-id-input" hidden>
        <input type="submit" value="submit" id="submit-btn" class="btn btn-primary hidden">
    </form>
